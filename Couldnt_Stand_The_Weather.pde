/**
 * Couldn't Stand The Weather.
 * draw vector field with De Jong attractor formula.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.05.19
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noStroke();
  noLoop();

}

void draw() {

  float hueInit     = random(360.0);
  float plotDiv     = 0.0005;
  int   paintCntMax = 2;

  for (int imgCnt = 1; imgCnt <= 3; ++imgCnt) {

    // shape parameters
    float paramA = random(5.0, 8.0);
    float paramB = random(1.0, 3.0);
    float paramC = random(5.0, 8.0);
    float paramD = random(1.0, 3.0);

    hueInit += 90.0;

    background(0.0, 0.0, 90.0, 100);
    for (int paintCnt = 1; paintCnt <= paintCntMax; ++paintCnt) {

      float paintRatio = map(paintCnt, 1, paintCntMax, 0.0, 1.0);

      int   plotCntMax = floor(map(paintRatio, 0.0, 1.0, 10.0, 200.0));
      float initDiv    = map(paintRatio, 0.0, 1.0, 0.0005, 0.005);
      float baseHue    = hueInit;
      float baseSat    = 100.0;
      float baseBri    = 60.0;
      float baseAlp    = map(paintRatio, 0.0, 1.0, 100.0, 70.0);
      float baseSiz    = 1.0;
     
      // draw vector field
      for (float xInit = 0.1; xInit <= 0.9; xInit += initDiv) {
        for (float yInit = 0.1; yInit <= 0.9; yInit += initDiv) {
          float xPoint = xInit;
          float yPoint = yInit;
          for (int plotCnt = 0; plotCnt < plotCntMax; ++plotCnt) {

            float plotRatio = map(plotCnt, 0, plotCntMax, 0.0, 1.0);
            float eHue      = baseHue + plotRatio * 60.0 + customNoise(xInit, yInit) * 30.0;
            float eSat      = baseSat * map(sin(PI * plotRatio), 0.0, 1.0, 1.0, 0.1);
            float eBri      = baseBri * sin(PI * plotRatio);
            float eSiz      = baseSiz * sin(PI * plotRatio);

            // De Jong attractors fromula
            float xPrev = xPoint;
            float yPrev = yPoint;
            xPoint += plotDiv * cos(TWO_PI * (sin(TWO_PI * paramA * yPrev) - cos(TWO_PI * paramB * xPrev)));
            yPoint += plotDiv * sin(TWO_PI * (sin(TWO_PI * paramC * xPrev) - cos(TWO_PI * paramD * yPrev)));

            fill(eHue % 360.0, eSat, eBri, baseAlp);
            ellipse(xPoint * width, yPoint * height, eSiz, eSiz);

          }
        }
      }
    }

    saveFrame("frames/" + String.format("%04d", imgCnt) + ".png");

  }

  exit();

}

/**
 * customNoise : returns -1.0 .. 1.0 almost random but interesting value
 */
float customNoise(float _x, float _y) {
  return pow(sin(TWO_PI * _x), 3) * cos(TWO_PI * pow(_y, 2));
}
